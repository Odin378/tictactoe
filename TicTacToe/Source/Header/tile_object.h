#pragma once
#include "string"

class Tile_Object
{
public:
	Tile_Object();
	virtual ~Tile_Object();
	void virtual drawObject();

protected:
	std::string spriteFile;
};

