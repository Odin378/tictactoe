#pragma once
#include "string"
class Experience_Item
{
public:
	Experience_Item(std::string name, int itemLevel = 50, double xpModifier = 1);
	virtual ~Experience_Item();

	std::string name;
	void addXP(int xp);
	void setLevel(int level);
	void setXPModifier(double xpModifier);
	int getItemLevel();
	int getXP();
	double getXPModifier();
	int getXPThreshold();

protected:
	int itemLevel, xp, xpThreshold;
	double xpModifier;
	void levelUp();
	void calculateXPThreshold();
	void checkXP();
};

