#pragma once

#include "SDL.h"
#include <string>
#include <map>
#include "color.h"

extern SDL_Window *window;
extern SDL_Renderer *render;


class Game_Shell
{
public:
	int sizeX, sizeY;
	static SDL_Window* window;
	static SDL_Renderer* render;

	Game_Shell(int x = 500, int y = 500);
	~Game_Shell();

	void startGame();

	static void setColor(std::string enumValue);
	static void setColor(std::string enumValue, Color* color);

	static void addColor(std::string colorName, Color* color);
	
	static void setGameEnd(bool gameEnd);
	static void debugFlash(std::string f1, std::string f2 ,std::string message = "No message");
	

protected:
	static bool gameEnd;
	typedef std::map<std::string, Color*> ColorMap;
	static ColorMap colors;
	static std::string currentColor;

private:
	void initSDL();
	void initColorMap();
	static int mainGameReference;
	static int getColorIndex(std::string enumName);
};