#pragma once
#include <string>
#include "grid_tile.h"

class Grid
{
public:
	Grid();
	virtual ~Grid();
	static void drawGrid(int x,int y, int gridWidth, int gridHeight);
	static void click(int x, int y);
protected:
private:
	static std::string gridColor;
	static int lineLength;
	static Grid_Tile tiles[][300];
	static Grid_Tile selectedTile;
	static int tileRows,tileColumns;
};

