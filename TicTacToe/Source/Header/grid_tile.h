#pragma once
#include "string"
#include "SDL.h"
#include <map>
#include "tile_object.h"

class Grid_Tile
{
public:
	int x, y, length;
	Grid_Tile();
	Grid_Tile(int x,int y,int lineLength);
	virtual ~Grid_Tile();

	void setGridTileLocation(int x, int y);
	void drawTile();
	bool checkCoords(int x, int y);
	void selectTile();
	void deselectTile();
	void addObject(Tile_Object* object);
	void placeObject(Tile_Object* object, int index);
	//Int is the Z index
	std::map<int, Tile_Object*> tileObjects;
	
protected:
	static std::string lineColor;
	static std::string selectedColor;
	static std::string wipeColor;
	void adjustMap(Tile_Object* object, int moveIndex);

	SDL_Rect getRect();

private:
	
};
