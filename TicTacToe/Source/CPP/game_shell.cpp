#include "game_shell.h"
#include "SDL.h"
#include "grid.h"
#include "color.h"
#include <iostream>
#include <map>
#include <string>

SDL_Window* Game_Shell::window = NULL;
SDL_Renderer* Game_Shell::render = NULL;

using namespace std;

//Old Code, couldn't get to work, keeping it for reference though
//map<string,Color*> colors = {
//	{"RED",new Color(255,0,0,0)},
//	{"BLUE",new Color(0,0,255,0)},
//	{"YELLOW",new Color(255,255,0,0)},
//	{"ORANGE", new Color(255,140,0,0)},
//	{"PURPLE", new Color(128,0,128,0)},
//	{"GREEN", new Color(0,128,0,0)},
//	{"WHITE", new Color(255,255,255,0)},
//	{"BLACK", new Color(0,0,0,0)},
//	{"GREY", new Color(119,135,153,0)}
//};
//string Game_Shell::currentColor;
Color* Game_Shell::drawColor = new Color(255, 255, 255, 0);
bool Game_Shell::gameEnd = false;


Game_Shell::Game_Shell(int x, int y) {
	this->sizeX = x;
	this->sizeY = y;
	this->gameEnd = false;
	initSDL();
	initColorMap();
}

void Game_Shell::initSDL() {
	SDL_Init(SDL_INIT_EVERYTHING);
	window = SDL_CreateWindow("Project Tyr", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, this->sizeX, this->sizeY, SDL_WINDOW_SHOWN);
	render = SDL_CreateRenderer(window, -1, 0);
}

void Game_Shell::initColorMap() {
}

void Game_Shell::startGame() {
	Game_Shell::setColor(new Color(0, 0, 0, 0));
	SDL_RenderClear(render);
	Grid::drawGrid(0, 0, 400, 400);
	SDL_RenderPresent(render);

	SDL_Event event;
	while (!Game_Shell::gameEnd) {
		while (SDL_PollEvent(&event)) {
			switch (event.type) {
			case SDL_QUIT:
				Game_Shell::setGameEnd(true);
				break;
			case SDL_MOUSEBUTTONDOWN:
				int x = event.button.x;
				int y = event.button.y;
				Grid::click(x, y);
				break;
			}
		}
	}
	SDL_Quit();
}

//Old Code, couldn't get it to work
//void Game_Shell::setColor(string color) {
//	if (colors.find(color) == colors.end()) {
//		std::cout << "Color: " << color << " missing";
//	}
//	else {
//		Color* colorObj = Game_Shell::colors[color];
//		//render object, r,g,b,a
//		SDL_SetRenderDrawColor(render, colorObj->colorInfo[0], colorObj->colorInfo[1], colorObj->colorInfo[2], colorObj->colorInfo[3]);
//		Game_Shell::currentColor = color;
//	}
//}

void Game_Shell::setColor(Color* colorObj) {
	Game_Shell::drawColor = colorObj;
	int red = colorObj->colorInfo[0];
	SDL_SetRenderDrawColor(render, colorObj->colorInfo[0], colorObj->colorInfo[1], colorObj->colorInfo[2], colorObj->colorInfo[3]);
	cout << red << endl;
}

void Game_Shell::setGameEnd(bool gameEnd) {
	Game_Shell::gameEnd = gameEnd;
}

//Old code, couldn't get it to work
//void Game_Shell::debugFlash(string f1, string f2,string message) {
//	std::string prevColor = Game_Shell::currentColor;
//	cout << "Debug Flash Start: "<<message<<endl;
//	Game_Shell::setColor(f1);
//	SDL_RenderClear(render);
//	SDL_RenderPresent(render);
//	SDL_Delay(500);
//	Game_Shell::setColor(f2);
//	SDL_RenderClear(render);
//	SDL_RenderPresent(render);
//	SDL_Delay(500);
//	Game_Shell::setColor(f1);
//	SDL_RenderClear(render);
//	SDL_RenderPresent(render);
//	SDL_Delay(500);
//	Game_Shell::setColor("BLACK");
//	SDL_RenderClear(render);
//	SDL_RenderPresent(render);
//	Game_Shell::setColor(prevColor);
//
//}


Game_Shell::~Game_Shell()
{
}
