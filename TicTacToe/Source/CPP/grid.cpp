#include "grid.h"
#include "SDL.h"
#include "game_shell.h"
#include "grid_tile.h"
#include <iostream>

using namespace std;

Color* Grid::gridColor = new Color(0, 0, 255, 0);
int Grid::lineLength = 50;

//First is row number, Second is column number
Grid_Tile Grid::tiles[300][300];
Grid_Tile Grid::selectedTile = Grid::tiles[0][0];

//For purposes later, may not be needed but they are here
int Grid::tileRows = 0;
int Grid::tileColumns = 0;

Grid::Grid() {

}

void Grid::click(int x, int y) {
	bool found = false;
	for (int i = 0; i < tileRows; i++) {
		for (int j = 0; j < tileColumns; j++) {
			if (Grid::tiles[i][j].checkCoords(x, y)) {
				//Grid::selectedTile.deselectTile();
				//Grid::tiles[i][j].selectTile();
				Grid::selectedTile = Grid::tiles[i][j];
				found = true;
				break;
			}
		}
		if (found) {
			break;
		}
	}
}

void Grid::drawGrid(int x, int y, int gridWidth, int gridHeight) {
	int yIterations = gridHeight / lineLength;
	int xIterations = gridWidth / lineLength;
	Game_Shell::setColor(gridColor);
	Grid::tileRows = yIterations;
	Grid::tileColumns = xIterations;

	for (int i = 0; i < yIterations; i++) {
		for (int j = 0; j < xIterations; j++) {
			int tempX = x + (lineLength * j);
			int tempY = y + (lineLength * i);
			Grid_Tile tile; tile.setGridTileLocation(tempX, tempY);
			tile.drawTile();
			Grid::tiles[i][j] = tile;
		}
	}

}



Grid::~Grid()
{
}
