#include "grid_tile.h"
#include "game_shell.h"
#include "string"
#include "color.h"
#include <iostream>
#include <map>
#include "tile_object.h"


Color* Grid_Tile::lineColor = new Color(0, 0, 255, 0);
//std::string Grid_Tile::selectedColor = "PURPLE";
//std::string Grid_Tile::wipeColor = "BLACK";

Grid_Tile::Grid_Tile(int x, int y, int lineLength) {
	this->x = x;
	this->y = y;
	this->length = lineLength;
	std::map<int, Tile_Object> tileObjects;
}

Grid_Tile::Grid_Tile() {
	this->x = 0;
	this->y = 0;
	this->length = 0;
}

void Grid_Tile::addObject(Tile_Object* object) {
	//Grab the Key/Index
	int last = tileObjects.end()->first;
}

void Grid_Tile::placeObject(Tile_Object* object, int index) {
	std::map<int, Tile_Object*>::iterator it = tileObjects.find(index);
	if (it == tileObjects.end()) {
		tileObjects[index] = object;
	}
	else {
		this->adjustMap(object, index);
	}
}

void Grid_Tile::adjustMap(Tile_Object* object, int moveIndex) {
	if (tileObjects.count(moveIndex) == 0) {

	}
	else {
		Tile_Object* tempObject = tileObjects[moveIndex];
		tileObjects[moveIndex] = object;
		adjustMap(tempObject, (moveIndex + 1));
	}
}

void Grid_Tile::drawTile() {
	Game_Shell::setColor(Grid_Tile::lineColor);
	SDL_RenderDrawLine(Game_Shell::render, x, y, x + this->length, y);
	SDL_RenderDrawLine(Game_Shell::render, x, y, x, y + this->length);
	SDL_RenderDrawLine(Game_Shell::render, x + this->length, y, x + this->length, y + this->length);
	SDL_RenderDrawLine(Game_Shell::render, x, y + this->length, x + this->length, y + this->length);
	SDL_RenderPresent(Game_Shell::render);
}

void Grid_Tile::setGridTileLocation(int x, int y) {
	this->x = x;
	this->y = y;
}

bool Grid_Tile::checkCoords(int x, int y) {
	if (x >= this->x && x <= this->x + this->length) {
		if (y >= this->y && y <= this->y + this->length) {
			return true;
		}
	}
	return false;
}

//Functions made for other things, should probably cut them out at some point

//void Grid_Tile::selectTile() {
//	Game_Shell::setColor(Grid_Tile::selectedColor);
//	SDL_Rect tile = this->getRect();
//	SDL_RenderFillRect(Game_Shell::render, &tile);
//	SDL_RenderPresent(Game_Shell::render);
//}
//
//void Grid_Tile::deselectTile() {
//	Game_Shell::setColor(Grid_Tile::wipeColor);
//	SDL_Rect tile = this->getRect();
//	SDL_RenderFillRect(Game_Shell::render, &tile);
//	SDL_RenderPresent(Game_Shell::render);
//	this->drawTile();
//}

SDL_Rect Grid_Tile::getRect() {
	SDL_Rect tile;
	tile.x = this->x;
	tile.y = this->y;
	tile.w = this->length;
	tile.h = this->length;
	return tile;
}


Grid_Tile::~Grid_Tile()
{
}
